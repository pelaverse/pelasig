
Installation/ Uninstallation
----------------------------

Plugin installation
"""""""""""""""""""
.. image:: _images/1install.png
    :width: 100%
    :align: center

Plugin uninstallation
""""""""""""""""""""""

.. image:: _images/1desinstall.png
    :width: 100%
    :align: center
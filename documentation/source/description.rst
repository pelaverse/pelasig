
Plugin description
""""""""""""""""""

*Processing observational and effort data for the census of marine megafauna.*

The pelaSIG plugin provides several tools to prepare, analyze and map data collected by aerial or boat surveys

|

This plugin provides the following tools:

- **1   Help** : link to the plugin's manual
- **2   Prepare files for SAMMOA** : prepare transect and strata files in conformity with SAMMOA software requirements
- **3   Check files**: lists the checks and corrections to be made to validate the data (effort and observation)
- **4   Linearize effort** : linearize points of effort collect during survey and prepare the sighting file for analysis
- **5   Segment linearized effort** : segment legs into segments
- **6   Analyse** : launches descriptive analyses on the data
- **7   Mapping** : create different type of maps
- **8   Import into database**  : import data from shapefiles into a database
- **9   Share data**  : export data to share it


|

.. image:: _images/1descriptiontools.png
    :width: 100%
    :align: center
|

pelaSIG plugin is accessible from the menu bar:

|

.. image:: _images/4menuplugin.png
    :width: 40%
    :align: center

|

And from a toolbar :


.. image:: _images/5toolbarplugin.png
    :width: 40%
    :align: center

|


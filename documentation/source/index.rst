

Introduction
============

pelaGIS is a QGis plugin written in Python 3.x. This plugin has been tested in QGis 3.16.5 version.

This plugin is a complementary tools to the SAMMOA software whose tutorial installation is available in `PDF <_static/tutoriel_sammoa_Installation_2018.pdf>`_.

.. toctree::
    :maxdepth: 2
    :titlesonly:

    install
    description
    prepare
    check
    linear
    segment
    analyze
    map
    import
    share


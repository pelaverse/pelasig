
Prepare files for SAMMOA software
"""""""""""""""""""""""""""""""""

.. image:: _images/1prepare.png
    :width: 100%
    :align: center

.. image:: _images/2preparecheck.png
    :width: 100%
    :align: center

.. image:: _images/3preparecheck.png
    :width: 100%
    :align: center

.. image:: _images/4prepareresult.png
    :width: 100%
    :align: center
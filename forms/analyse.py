# -*- coding: utf-8 -*-

#  Copyright (c) $Manon Niviere, Sophie Laran, Oriane Penot, Adrien Gatineau, Alain Layec. 2020. Observatoire Pelagis

# Form implementation generated from reading ui file 'analyse.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_analyse_form(object):
    def setupUi(self, analyse_form):
        analyse_form.setObjectName("analyse_form")
        analyse_form.resize(1028, 601)
        self.verticalLayout = QtWidgets.QVBoxLayout(analyse_form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label = QtWidgets.QLabel(analyse_form)
        self.label.setMinimumSize(QtCore.QSize(120, 0))
        self.label.setObjectName("label")
        self.horizontalLayout_4.addWidget(self.label)
        self.le_sigh = QtWidgets.QLineEdit(analyse_form)
        self.le_sigh.setMaximumSize(QtCore.QSize(16777215, 30))
        self.le_sigh.setObjectName("le_sigh")
        self.horizontalLayout_4.addWidget(self.le_sigh)
        self.pb_sigh_file = QtWidgets.QPushButton(analyse_form)
        self.pb_sigh_file.setMaximumSize(QtCore.QSize(16777215, 30))
        self.pb_sigh_file.setObjectName("pb_sigh_file")
        self.horizontalLayout_4.addWidget(self.pb_sigh_file)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.lbl_select_trans = QtWidgets.QLabel(analyse_form)
        self.lbl_select_trans.setMinimumSize(QtCore.QSize(120, 0))
        self.lbl_select_trans.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.lbl_select_trans.setObjectName("lbl_select_trans")
        self.horizontalLayout_3.addWidget(self.lbl_select_trans)
        self.le_effort = QtWidgets.QLineEdit(analyse_form)
        self.le_effort.setMaximumSize(QtCore.QSize(16777215, 30))
        self.le_effort.setObjectName("le_effort")
        self.horizontalLayout_3.addWidget(self.le_effort)
        self.pb_effort_file = QtWidgets.QPushButton(analyse_form)
        self.pb_effort_file.setMaximumSize(QtCore.QSize(16777215, 30))
        self.pb_effort_file.setObjectName("pb_effort_file")
        self.horizontalLayout_3.addWidget(self.pb_effort_file)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.cb_analyse = QtWidgets.QComboBox(analyse_form)
        self.cb_analyse.setMinimumSize(QtCore.QSize(0, 32))
        self.cb_analyse.setObjectName("cb_analyse")
        self.horizontalLayout.addWidget(self.cb_analyse)
        self.pb_compute = QtWidgets.QPushButton(analyse_form)
        self.pb_compute.setMinimumSize(QtCore.QSize(150, 32))
        self.pb_compute.setMaximumSize(QtCore.QSize(100, 32))
        self.pb_compute.setObjectName("pb_compute")
        self.horizontalLayout.addWidget(self.pb_compute)
        self.pb_export = QtWidgets.QPushButton(analyse_form)
        self.pb_export.setMinimumSize(QtCore.QSize(150, 32))
        self.pb_export.setMaximumSize(QtCore.QSize(100, 32))
        self.pb_export.setObjectName("pb_export")
        self.horizontalLayout.addWidget(self.pb_export)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.tablew_result = QtWidgets.QTableWidget(analyse_form)
        self.tablew_result.setObjectName("tablew_result")
        self.tablew_result.setColumnCount(0)
        self.tablew_result.setRowCount(0)
        self.verticalLayout.addWidget(self.tablew_result)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem = QtWidgets.QSpacerItem(672, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pb_cancel = QtWidgets.QPushButton(analyse_form)
        self.pb_cancel.setObjectName("pb_cancel")
        self.horizontalLayout_2.addWidget(self.pb_cancel)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(analyse_form)
        QtCore.QMetaObject.connectSlotsByName(analyse_form)

    def retranslateUi(self, analyse_form):
        _translate = QtCore.QCoreApplication.translate
        analyse_form.setWindowTitle(_translate("analyse_form", "Data analysis"))
        self.label.setText(_translate("analyse_form", "Select sighting file"))
        self.pb_sigh_file.setText(_translate("analyse_form", "..."))
        self.lbl_select_trans.setText(_translate("analyse_form", "Select linear file"))
        self.pb_effort_file.setText(_translate("analyse_form", "..."))
        self.pb_compute.setText(_translate("analyse_form", "Compute"))
        self.pb_export.setText(_translate("analyse_form", "Export in CSV"))
        self.pb_cancel.setText(_translate("analyse_form", "Cancel"))


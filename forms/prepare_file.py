# -*- coding: utf-8 -*-

#  Copyright (c) $Manon Niviere, Sophie Laran, Oriane Penot, Adrien Gatineau, Alain Layec. 2020. Observatoire Pelagis

# Form implementation generated from reading ui file 'prepare_file.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_prepare_file(object):
    def setupUi(self, prepare_file):
        prepare_file.setObjectName("prepare_file")
        prepare_file.resize(804, 393)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(prepare_file)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.lbl_export = QtWidgets.QLabel(prepare_file)
        self.lbl_export.setMinimumSize(QtCore.QSize(120, 0))
        self.lbl_export.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.lbl_export.setObjectName("lbl_export")
        self.horizontalLayout_3.addWidget(self.lbl_export)
        self.lE_export = QtWidgets.QLineEdit(prepare_file)
        self.lE_export.setMaximumSize(QtCore.QSize(16777215, 30))
        self.lE_export.setObjectName("lE_export")
        self.horizontalLayout_3.addWidget(self.lE_export)
        self.pB_export_dir = QtWidgets.QPushButton(prepare_file)
        self.pB_export_dir.setMaximumSize(QtCore.QSize(16777215, 30))
        self.pB_export_dir.setObjectName("pB_export_dir")
        self.horizontalLayout_3.addWidget(self.pB_export_dir)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.lbl_select_trans = QtWidgets.QLabel(prepare_file)
        self.lbl_select_trans.setMinimumSize(QtCore.QSize(120, 0))
        self.lbl_select_trans.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.lbl_select_trans.setObjectName("lbl_select_trans")
        self.horizontalLayout.addWidget(self.lbl_select_trans)
        self.lE_trans = QtWidgets.QLineEdit(prepare_file)
        self.lE_trans.setMaximumSize(QtCore.QSize(16777215, 30))
        self.lE_trans.setObjectName("lE_trans")
        self.horizontalLayout.addWidget(self.lE_trans)
        self.pB_transect_file = QtWidgets.QPushButton(prepare_file)
        self.pB_transect_file.setMaximumSize(QtCore.QSize(16777215, 30))
        self.pB_transect_file.setObjectName("pB_transect_file")
        self.horizontalLayout.addWidget(self.pB_transect_file)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.lbl_select_strat = QtWidgets.QLabel(prepare_file)
        self.lbl_select_strat.setMinimumSize(QtCore.QSize(120, 0))
        self.lbl_select_strat.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.lbl_select_strat.setObjectName("lbl_select_strat")
        self.horizontalLayout_2.addWidget(self.lbl_select_strat)
        self.lE_strat = QtWidgets.QLineEdit(prepare_file)
        self.lE_strat.setMaximumSize(QtCore.QSize(16777215, 30))
        self.lE_strat.setObjectName("lE_strat")
        self.horizontalLayout_2.addWidget(self.lE_strat)
        self.pB_strat_file = QtWidgets.QPushButton(prepare_file)
        self.pB_strat_file.setMaximumSize(QtCore.QSize(16777215, 30))
        self.pB_strat_file.setObjectName("pB_strat_file")
        self.horizontalLayout_2.addWidget(self.pB_strat_file)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.pB_create_file = QtWidgets.QPushButton(prepare_file)
        self.pB_create_file.setObjectName("pB_create_file")
        self.verticalLayout.addWidget(self.pB_create_file)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.lbl_result = QtWidgets.QLabel(prepare_file)
        self.lbl_result.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.lbl_result.setText("")
        self.lbl_result.setObjectName("lbl_result")
        self.verticalLayout_2.addWidget(self.lbl_result)

        self.retranslateUi(prepare_file)
        QtCore.QMetaObject.connectSlotsByName(prepare_file)

    def retranslateUi(self, prepare_file):
        _translate = QtCore.QCoreApplication.translate
        prepare_file.setWindowTitle(_translate("prepare_file", "Prepare files compatible with SAMMOA"))
        self.lbl_export.setText(_translate("prepare_file", "Export directory"))
        self.pB_export_dir.setText(_translate("prepare_file", "..."))
        self.lbl_select_trans.setText(_translate("prepare_file", "Select transect file"))
        self.pB_transect_file.setText(_translate("prepare_file", "..."))
        self.lbl_select_strat.setText(_translate("prepare_file", "Select strata file"))
        self.pB_strat_file.setText(_translate("prepare_file", "..."))
        self.pB_create_file.setText(_translate("prepare_file", "Create files "))


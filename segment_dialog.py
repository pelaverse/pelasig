# -*- coding: utf-8 -*-

"""
Segment linearized effort.

__copyright__ = "Copyright 2022, Observatoire Pelagis"
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

#  Copyright (c) $2022. Observatoire Pelagis

# --------------------------------------------------------------------------------
# ============================= Imports Python ===================================

import processing
import math

# ============================= Imports QGis =====================================
from qgis.core import QgsExpression, QgsMessageLog, QgsVectorFileWriter, QgsVectorLayer, QgsProject, QgsWkbTypes, \
    QgsFeatureRequest, QgsCoordinateReferenceSystem, Qgis, QgsField, edit, QgsExpressionContext, QgsFeature, \
    QgsDistanceArea, QgsUnitTypes
from PyQt5.QtWidgets import QWidget, QDialog, QFileDialog, QMessageBox
# ============================= Imports Modules internes =====================================
from .forms.segment_form import Ui_SegmentDialog
from .linear_dialog import *

# --------------------------------------------------------------------------------


class SegmentDialog(QDialog, Ui_SegmentDialog):

    def __init__(self, interface):

        QWidget.__init__(self)
        self.setupUi(self)

        self.interface = interface

        # CONNECTION DES BOUTONS

        self.pb_export.clicked.connect(self.open_export_dir)
        self.pb_linear.clicked.connect(self.open_effort_file)
        self.pb_sigh.clicked.connect(self.open_sigh_file)

        self.pb_run.clicked.connect(self.run)

        self.sb_sgt.setValue(10)
        self.sb_sgt.setMaximum(1000)

        # DECLARATIONS

        self.raw_dir = ""  # rawdata directory
        self.result_dir = ""  # result directory
        self.export_dir = ""  # export directory
        self.working_dir = ""  # working directory
        self.sigh_path = ""  # sighting path
        self.linear_path = ""  # effort path
        self.export_path = ""  # export path
        self.linear_copy = ""  # path of the copy of effort file
        self.linear_df = ""  # copy of effort file in csv
        self.linear_sort = ""  # fichier effort csv ordonné
        self.linear_lyr = None  # effort layer (copy of original)
        self.sgt_length = 0   # theoretical segment length
        self.errors = []  # liste des erreurs bloquant le lancement des algorithmes
        self.fields_list = []  # liste des attributs
        self.segments = []  # liste des segments créés
        self.nb_segment = 0  # nombre de segments
        self.split_legs = []  # liste des legs
        self.sgtL_calc = 0  # longueur finale des segments après avoir redistribué le remainder
        self.seg_lyr = None  # segment layer
        self.linear_sorted = None  # linearized effort sorted
        self.centroid_lyr = None  # centroides layer
        self.sigh_lyr = None  # sighting layer

        self.totalSteps = 1  # nombre total de pas pour remplir la barre de progression
        self.step = 0  # nombre effection de pas remplis

    # #######################################################################################################################
    # ----------------------------------CHECK FILES

    def check_dir(self):

        """
        Check directory selection
        :return:
        """

        if self.le_export.text() != "":
            QgsMessageLog.logMessage('Working directory is %s' % self.le_export.text(), level=Qgis.Info)

        else:
            self.errors.append("Export directory has to be specified.")

        return self.errors

    # -----------------------------------------------------

    def check_lin(self):

        """
        Check linear file selection
        :return:
        """

        if self.le_linear.text() != "":
            QgsMessageLog.logMessage('Linear shapefile source is %s' % self.le_linear.text(), level=Qgis.Info)

        else:
            self.errors.append("Linear file is missing.")

        return self.errors

    # -----------------------------------------------------

    def check_effort_geom(self, layer):

        """"
        Check if effort file has a point geometry
        :return:
        """

        msg_error = []
        if layer.geometryType() == QgsWkbTypes.LineGeometry:
            lin_crs = layer.crs()
            if lin_crs.mapUnits() == 0:  # mètres
                geom_line = True
            else:
                geom_line = False
                msg_error.append("The effort file must be in a metric projection system.")
        else:
            geom_line = False
            msg_error.append("Effort geometry is not LineGeometry but {0}.".format(layer.geometryType()))

        return geom_line, msg_error

    # ----------------- OUVERTURE DES FICHIERS PAR L'UTILISATEUR

    def open_export_dir(self):

        """
        Display path of selected directory
        :return:
        """

        self.export_dir = QFileDialog.getExistingDirectory(self, 'Select export directory', "C:/")
        self.le_export.setText(self.export_dir)

    # -----------------------------------------------------

    def open_effort_file(self):
        """
        Display effort file path in label
        :return: effort_path
        """

        effort_path = QFileDialog.getOpenFileName(self, "Select effort file",
                                                  self.export_dir, "Shapefile or Geopackage (*.shp; *.gpkg)")
        self.le_linear.setText(effort_path[0])

        return effort_path

    # -----------------------------------------------------

    def open_sigh_file(self):
        """
        Display sighting file path in label
        :return: sigh_path
        """

        sigh_path = QFileDialog.getOpenFileName(self, "Select sighting file",
                                                      self.export_dir, "Shapefile  or Geopackage (*.shp; *.gpkg)")
        self.le_sigh.setText(sigh_path[0])

        return sigh_path

    # ------------------ VARIABLES DEFINIES PAR L'UTILISATEUR

    def get_sgt_length(self):

        """
        Get segment length.
        :return:self.sgt_length
        """

        self.sgt_length = self.sb_sgt.value()

        return self.sgt_length

    # ------------------- FONCTION BOUTON RUN

    def run(self):
        """
        Run all checks
        :return:
        """

        self.updateProgressBar()
        self.step = 0
        self.errors = []

        self.check_dir()
        self.check_lin()

        self.fields_list = []

        if self.errors:
            QMessageBox.critical(None, "Error", '\n'.join(self.errors))  # affiche les messages d'erreurs

        else:
            self.export_path = self.le_export.text()  # chemin du répertoire d'export
            self.linear_path = self.le_linear.text()  # chemin du fichier effort linéarisé
            self.sigh_path = self.le_sigh.text()  # chemin du fichier effort linéarisé

            # création des sous-répertoires
            self.raw_dir = LinearDialog.init_dir(self, 'rawdata_seg')
            self.working_dir = LinearDialog.init_dir(self, 'working_files')
            self.result_dir = LinearDialog.init_dir(self, 'output')

            # TRAITEMENTS EFFORT LINEAIRE
            if os.path.isfile(self.linear_path) is True:
                # Copie du fichier d'origine dans le répertoire rawdata_seg
                self.linear_copy = self.copy_file(self.linear_path, self.raw_dir)
                linear_name = os.path.basename(self.linear_path).split('.')[0]
                self.linear_lyr = self.open_shp(self.linear_copy, linear_name, False)

                geom_line = self.check_effort_geom(self.linear_lyr)[0]
                if geom_line:

                    self.totalSteps = self.linear_lyr.featureCount() + 1
                    sgtL_th = self.get_sgt_length()  # taille des segments demandé par l'utilisateur
                    self.segments = []  # liste des segments créés

                    for field in self.linear_lyr.fields():
                        if field.name() == 'join_lid':
                            self.join_field = 'join_lid'
                        elif field.name() == 'joinlid':
                            self.join_field = 'joinlid'

                    # découpe le fichier d'effort par legs pour pouvoir les traités indépendamment
                    algo_split = processing.run("qgis:splitvectorlayer", {
                        'INPUT': self.linear_lyr, 'FIELD': self.join_field, 'FILE_TYPE': 0, 'OUTPUT': 'TEMPORARY_OUTPUT'})
                    self.split_legs = algo_split['OUTPUT_LAYERS']
                    QgsMessageLog.logMessage('SPLIT LEGS COMPLETED.')

                    # pour chaque leg
                    for leg in self.split_legs:
                        vlayer = QgsVectorLayer(leg, "seg_" + os.path.basename(leg), "ogr")
                        idx_legL = vlayer.fields().indexFromName('legLengKm')

                        for feat in vlayer.getFeatures():
                            l_leg = feat[idx_legL]  # longueur du leg calculée avec l'outil de linéarisation

                            if l_leg > sgtL_th:
                                self.nb_segment = int(l_leg // sgtL_th)  # quotient de la division

                                # calcul de la taille des segments au sein du leg
                                remainder = math.fmod(l_leg,
                                                      sgtL_th)  # pour un nombre décimal mieux vaut utiliser fmod que le modulo pour récupérer le reste d'une division

                                if remainder > sgtL_th / 2:
                                    self.sgtL_calc = sgtL_th
                                    self.nb_segment += 1
                                else:
                                    slice_leg = remainder / self.nb_segment
                                    self.sgtL_calc = sgtL_th + slice_leg  # longueur finale des segments après avoir redistribué le remainder
                                # QgsMessageLog.logMessage('Final segment length : {0}'.format(self.sgtL_calc))
                                algo_split = processing.run("native:splitlinesbylength",
                                                            {'INPUT': vlayer,
                                                             'LENGTH': self.sgtL_calc * 1000,
                                                             'OUTPUT': 'TEMPORARY_OUTPUT'})

                                self.split_leg = algo_split['OUTPUT']

                            else:
                                self.nb_segment = 1
                                self.split_leg = vlayer

                            # QgsMessageLog.logMessage('NB SEGMENT: {0}'.format(self.nb_segment))

                            with edit(self.split_leg):
                                self.split_leg.dataProvider().addAttributes([QgsField('DTstart', QVariant.String)])
                                self.split_leg.dataProvider().addAttributes([QgsField('DTend', QVariant.String)])
                                self.split_leg.dataProvider().addAttributes([QgsField('segLengKm', QVariant.Double, "double", 4, 2)])
                                self.split_leg.dataProvider().addAttributes([QgsField('segID', QVariant.Int)])
                                self.split_leg.updateFields()

                            idxdts = self.split_leg.fields().indexFromName("DTstart")
                            idxdte = self.split_leg.fields().indexFromName("DTend")
                            idxlenS = self.split_leg.fields().indexFromName("segLengKm")
                            idxdt2 = self.split_leg.fields().indexFromName("DATE_TIME2")

                            request = QgsFeatureRequest()
                            # order by date (pour avoir les pts dans le bon ordre du premier au dernier sur le leg)
                            clause = QgsFeatureRequest.OrderByClause('datehhmmss', ascending=True)
                            orderby = QgsFeatureRequest.OrderBy([clause])
                            request.setOrderBy(orderby)
                            features = self.split_leg.getFeatures(request)

                            time2add = 0

                            self.split_leg.startEditing()
                            for f in features:
                                context = QgsExpressionContext()
                                context.setFeature(f)

                                lengthSegExp = QgsExpression(''' $length/1000 ''')
                                lengthSeg = lengthSegExp.evaluate(context)
                                f[idxlenS] = lengthSeg
                                self.split_leg.updateFeature(f)

                                # en utilisant la vitesse moyenne sur le leg

                                deltaseg = QgsExpression('''{0}/("legLengKm"/second(to_datetime("DATE_TIME2") - to_datetime("DATE_TIME1")))'''.format(lengthSeg))
                                deltaT = deltaseg.evaluate(context)
                                expstseg = QgsExpression(
                                    '''to_datetime("DATE_TIME1")+to_interval('{0} second')'''.format(time2add))
                                expenseg = QgsExpression('''(
                                to_datetime("DATE_TIME1")+to_interval('{0} second'))+to_interval('{1} second')'''.format(
                                    time2add, deltaT))

                                context = QgsExpressionContext()
                                context.setFeature(f)
                                DTstseg = expstseg.evaluate(context)
                                DTenseg = expenseg.evaluate(context)

                                self.split_leg.updateFeature(f)

                                # start
                                f[idxdts] = DTstseg
                                # end
                                f[idxdte] = DTenseg

                                time2add += deltaT

                                self.split_leg.updateFeature(f)

                                # corriger DTend enfonction de DT2
                                test_diff = QgsExpression(''' second(to_datetime("DATE_TIME2") - to_datetime("DTend")) ''')

                                context = QgsExpressionContext()
                                context.setFeature(f)
                                diff_s = test_diff.evaluate(context)
                                if diff_s < 3:
                                    f[idxdte] = f[idxdt2]
                                self.split_leg.updateFeature(f)

                            self.split_leg.commitChanges()


                            # stocker les segments
                            for feat in self.split_leg.getFeatures():
                                geom = feat.geometry()
                                attrs = feat.attributes()
                                the_feature = QgsFeature()
                                the_feature.setGeometry(geom)
                                the_feature.setAttributes(attrs)
                                self.segments.append(the_feature)

                        self.fields_list = self.split_leg.dataProvider().fields().toList()

                        QgsProject.instance().removeMapLayer(self.split_leg.id())
                        QgsProject.instance().removeMapLayer(vlayer.id())

                        self.step += 1
                        self.updateProgressBar()

                    # créer une seule couche avec tous les segments
                    path_out = os.path.join(self.working_dir, 'SEG_' + linear_name)
                    merged_seg = QgsVectorLayer('MultiLineString?crs=' + 'epsg:'+self.linear_lyr.crs().authid(), 'SEG_' + linear_name,
                                                "memory")
                    save_options = QgsVectorFileWriter.SaveVectorOptions()
                    save_options.driverName = "ESRI Shapefile"
                    save_options.fileEncoding = "UTF-8"
                    transform_context = QgsProject.instance().transformContext()

                    _writer, error_string = QgsVectorFileWriter.writeAsVectorFormatV2(
                        merged_seg, path_out, transform_context, save_options)

                    if _writer == QgsVectorFileWriter.NoError:
                        QgsMessageLog.logMessage('No writer error (%s).' % path_out, level=Qgis.Info)
                        self.seg_lyr = self.open_shp(
                            os.path.join(self.working_dir, 'SEG_'+linear_name + '.shp'),
                            'SEG_' + linear_name, False)

                        # Add the features to the merged layer
                        provider = self.seg_lyr.dataProvider()
                        provider.addAttributes(self.fields_list)
                        self.seg_lyr.updateFields()

                        self.seg_lyr.startEditing()
                        provider.addFeatures(self.segments)
                        self.seg_lyr.commitChanges()

                        # sort by attribute
                        linear_sorted_path = os.path.join(self.result_dir, 'SEG_' + linear_name + '_SegOrd.shp')
                        LinearDialog.sort(self, self.seg_lyr, 'datehhmmss', linear_sorted_path,
                                          QgsCoordinateReferenceSystem(self.linear_lyr.crs()))
                        self.linear_sorted = self.open_shp(linear_sorted_path, 'SEG_' + linear_name + '_SegOrd', True)

                        # delete fid to resolve pb of wrong fid field type
                        attr_lin = []
                        for field in self.linear_sorted.fields():
                            attr_lin.append(field.name())

                        if 'fid' in attr_lin:
                            idxfid = self.linear_sorted.fields().indexFromName("fid")
                            self.linear_sorted.dataProvider().deleteAttributes([idxfid])
                            self.linear_sorted.updateFields()

                        if 'gid' in attr_lin:
                            idxfid = self.linear_sorted.fields().indexFromName("gid")
                            self.linear_sorted.dataProvider().deleteAttributes([idxfid])
                            self.linear_sorted.updateFields()

                        # nettoyer les segments restes créé par l'outil découpe en segment de QGis

                        prov = self.linear_sorted.dataProvider()  # Define the provider
                        request = ''' "legLengKm">5 and "segLengKm"<4.5 '''
                        it = self.linear_sorted.getFeatures(QgsFeatureRequest().setFilterExpression(request))
                        prov.deleteFeatures([i.id() for i in it])  # Delete the selected features
                        self.linear_sorted.removeSelection()


                        # itérer segID
                        idxsegid = self.linear_sorted.fields().indexFromName("segID")

                        expid = QgsExpression('''$id+1''')
                        with edit(self.linear_sorted):
                            for feature in self.linear_sorted.getFeatures():
                                context = QgsExpressionContext()
                                context.setFeature(feature)
                                feature[idxsegid] = expid.evaluate(context)
                                self.linear_sorted.updateFeature(feature)


                        # EXTRACT CENTROIDS OF EACH SEGMENTS
                        algo_centroids = processing.run("native:centroids",
                                                         {'INPUT': self.linear_sorted,
                                                          'ALL_PARTS': True,
                                                          'OUTPUT': 'TEMPORARY_OUTPUT'})
                        centroids = algo_centroids['OUTPUT']
                        centroid_output = os.path.join(self.result_dir, 'SEG_' + linear_name + '_centroids')
                        save_options = QgsVectorFileWriter.SaveVectorOptions()
                        save_options.fileEncoding = "UTF-8"
                        save_options.driverName = "ESRI Shapefile"
                        transform_context = QgsProject.instance().transformContext()
                        QgsVectorFileWriter.writeAsVectorFormatV2(centroids, centroid_output, transform_context, save_options)

                        self.centroid_lyr = self.open_shp(
                            os.path.join(self.result_dir, 'SEG_' + linear_name + '_centroids.shp'),
                            'SEG_' + linear_name + '_centroids', True)

                        self.step += 1

                        QgsProject.instance().removeMapLayer(self.linear_lyr.id())
                        QgsProject.instance().removeMapLayer(self.seg_lyr.id())

                        # SIGHTING FILE (get id_seg)

                        if self.sigh_path != "None" and self.sigh_path != '':
                            if os.path.isfile(self.sigh_path) is True:
                                sigh_copy = self.copy_file(self.sigh_path, self.result_dir)
                                sigh_rename = self.rename_file(sigh_copy, self.result_dir)
                                sigh_lyr_name = os.path.basename(sigh_rename)
                                self.sigh_lyr = self.open_shp(sigh_rename, sigh_lyr_name, True)

                                self.totalSteps += self.sigh_lyr.featureCount()
                                # ajout colonne segID
                                with edit(self.sigh_lyr):
                                    self.sigh_lyr.dataProvider().addAttributes([QgsField('segID', QVariant.Int)])
                                    self.sigh_lyr.updateFields()
                                self.sigh_lyr.commitChanges()

                                for field in self.sigh_lyr.fields():
                                    if field.name() == 'join_lid':
                                        self.join_field_obs = 'join_lid'
                                    elif field.name() == 'joinlid':
                                        self.join_field_obs = 'joinlid'

                                idxdt = self.sigh_lyr.fields().indexFromName('datehhmmss')
                                idxsid_obs = self.sigh_lyr.fields().indexFromName('segID')
                                idxjlid_obs = self.sigh_lyr.fields().indexFromName(self.join_field_obs)
                                idxdt1 = self.linear_sorted.fields().indexFromName('DTstart')
                                idxdt2 = self.linear_sorted.fields().indexFromName('DTend')
                                idxsid_seg = self.linear_sorted.fields().indexFromName('segID')

                                for feat in self.sigh_lyr.getFeatures():

                                    self.step += 1
                                    self.updateProgressBar()

                                    joinlid = feat[idxjlid_obs]

                                    self.linear_sorted.selectByExpression(''' "{0}"='{1}' '''.format(self.join_field, joinlid))

                                    for f in self.linear_sorted.selectedFeatures():
                                        if f[idxdt2] >= feat[idxdt] >= f[idxdt1]:
                                            with edit(self.sigh_lyr):
                                                feat[idxsid_obs] = f[idxsid_seg]
                                                self.sigh_lyr.updateFeature(feat)
                                        else:
                                            continue

                                self.sigh_lyr.commitChanges()

                else:
                    QMessageBox.critical(self, 'CRS ERROR', '{0}'.format(self.check_effort_geom(self.linear_lyr)[1]))

# _____________________________________________________________________________________________________

    # ------- OPEN SHAPEFILE IN QGIS

    def open_shp(self, shp, shp_name, openlyr):
        """
        Open shapefile in map register.
        :param shp:
        :param shp_name:
        :param openlyr:
        :return shapefile:
        """

        shapefile = QgsVectorLayer(shp, shp_name, "ogr")

        layer = ""

        if shapefile.isValid():
            QgsProject.instance().addMapLayer(shapefile, openlyr)  # False : add in registry only
            layer = QgsProject.instance().mapLayersByName(shp_name)[0]

        else:
            self.errors.append('This layer is not valid.')

        return layer

    # ------- COPY FILE

    def copy_file(self, shp_src, directory):
        """
        copy files from origin directory to export directory>rawdata.
        ::return:: dest_shp destination shapefile path
        """

        name_file = os.path.splitext(os.path.basename(shp_src))[0]
        dest_shp = ""

        for filename in os.listdir(os.path.dirname(shp_src)):
            if os.path.isfile(
                    os.path.join(os.path.dirname(shp_src),
                                 filename)):  # eliminate directory from the selection

                pattern = name_file + ".*"
                match = fnmatch.fnmatch(filename, pattern)

                if match:
                    file_path = os.path.join(os.path.dirname(shp_src), filename)
                    dest_shp = os.path.join(directory, filename)
                    shutil.copy2(file_path, dest_shp)

        QgsMessageLog.logMessage('Shapefile destination is %s' % dest_shp, level=Qgis.Info)

        return dest_shp

# ---------- RENAME FILE

    def rename_file(self, shp_src, directory):
        """
        Rename file.
        """

        name_file = os.path.splitext(os.path.basename(shp_src))[0]
        dest_shp = ""

        for filename in os.listdir(directory):
            if os.path.isfile(
                    os.path.join(os.path.dirname(shp_src),
                                 filename)):  # eliminate directory from the selection

                pattern = name_file + ".*"
                match = fnmatch.fnmatch(filename, pattern)

                if match:
                    file_path = os.path.join(directory, filename)
                    dest_shp = os.path.join(directory, 'SEG_'+ filename)
                    os.rename(file_path, dest_shp)

        QgsMessageLog.logMessage('Shapefile destination is %s' % dest_shp, level=Qgis.Info)

        return dest_shp

    # --- PROGRESS BAR

    def updateProgressBar(self):

        """
        update Progress bar widget
        :return:
        """

        self.progressBar.setValue(int(self.step * 100 / self.totalSteps))
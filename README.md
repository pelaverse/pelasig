# QGisPelaSIGPlugin
Plugin QGis for processing census data of marine megafauna.
Version QGis recommended for the use of the plugin: 3.16.5-Hannover

Licensed under the terms of GNU GPL v3 http://www.gnu.org/copyleft/gpl.html

A notice is accessible via the first module of the plugin.


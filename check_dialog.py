# -*- coding: utf-8 -*-

"""
Check files downloaded from Sammoa softwares.

__copyright__ = "Copyright 2022, Observatoire Pelagis"
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


#  Copyright (c) $2022. Observatoire Pelagis


# --------------------------------------------------------------------------------
# ============================= Imports Python ===================================
import os
import processing
from datetime import date
import pandas as pd
import math
# ============================= Imports PyQt =====================================
from PyQt5.QtWidgets import QWidget, QDialog, QFileDialog, QMessageBox
# ============================= Imports QGis =====================================
from qgis.core import QgsMessageLog, QgsVectorFileWriter, QgsVectorLayer, QgsProject, QgsCoordinateReferenceSystem, Qgis
# ============================= Imports Modules internes =====================================
from .forms.check_form import Ui_check_dialog
# --------------------------------------------------------------------------------


class CheckDialog(QDialog, Ui_check_dialog):

    def __init__(self, interface):

        QWidget.__init__(self)
        self.setupUi(self)

        self.interface = interface

        self.effort_layer = None  # effort layer to check
        self.effort_LEG = None  # only LEG in effort file
        self.obs_layer = None  # observation layer to check

        self.output_dir_effort = None  # Effort output directory selected by user where log files will be saved
        self.output_dir_obs = None  # Observation output directory selected by user where log files will be saved

        self.effort_path = ''  # chemin du fichier effort
        self.obs_path = ''  # chemin du fichier observation

        self.path_effort = ''  # chemin du fichier log effort
        self.path_obs = ''  # chemin du fichier log observation

        self.effort_df = None  # fichier csv effort
        self.obs_df = None  # fichier csv observation

        # CONNECTION DES BOUTONS

        self.pb_get_effort.clicked.connect(self.open_effort_file)
        self.pb_get_obs.clicked.connect(self.open_obs_file)
        self.pb_run.clicked.connect(self.run)

        self.totalSteps = 0  # nombre total de pas pour remplir la barre de progression
        self.step = 0  # nombre effection de pas remplis
        self.progressBar.setValue(0)

        pd.set_option('display.max_columns', 50)
        pd.set_option('display.width', 500)

    # #######################################################################################################################
    # ----------------- OUVERTURE DES FICHIERS PAR L'UTILISATEUR
    def open_effort_file(self):

        """
        Display effort file path in label
        :return: effort_path
        """

        effort_path = QFileDialog.getOpenFileName(self, "Select effort file",
                                                  'C:/', "Shapefile (*.shp)")
        self.lbl_effort_path.setText(effort_path[0])

        return effort_path

    # --------
    def open_obs_file(self):
        """
        Display observation file path in label
        :return: obs_path
        """

        obs_path = QFileDialog.getOpenFileName(self, "Select observation file",
                                                    'C:/', "Shapefile (*.shp)")
        self.lbl_obs_path.setText(obs_path[0])

        return obs_path

    # ------- OPEN SHAPEFILE IN QGIS
    def open_shp(self, shp, shp_name, openlyr):
        """
        Open shapefile in map register.
        :param shp:
        :param shp_name:
        :param openlyr:
        :return shapefile:
        """

        shapefile = QgsVectorLayer(shp, shp_name, "ogr")

        layer = ""
        if shapefile.isValid():
            QgsProject.instance().addMapLayer(shapefile, openlyr)  # False : add in registry only
            layer = QgsProject.instance().mapLayersByName(shp_name)[0]

        else:
            self.errors.append('This layer is not valid.')

        return layer

    # ------- MET A JOUR LA BARRE DE PROGRESSION

    def updateProgressBar(self):
        """
        update Progress bar widget
        :return:
        """

        self.progressBar.setValue(int(self.step * 100 / self.totalSteps))

    # ------- FONCTION BOUTON RUN

    def run(self):
        """
        Run all checks
        :return:
        """

        self.totalSteps = 0
        self.step = 0

        self.effort_path = self.lbl_effort_path.text()
        self.obs_path = self.lbl_obs_path.text()

        if self.effort_path != '':

            self.output_dir_effort = os.path.dirname(self.effort_path)
            QgsMessageLog.logMessage('OUTPUT DIRECTORY FOR EFFORT LOG: {0}'.format(self.output_dir_effort))

            # open files in qgis registry
            name_file = os.path.splitext(os.path.basename(self.effort_path))[0]
            self.effort_layer = self.open_shp(self.effort_path, 'check_'+name_file, False)
            # sélection des legs
            self.effort_layer.selectByExpression(''' "routeType" = 'LEG' ''', QgsVectorLayer.SetSelection)
            algo_save = processing.run('qgis:saveselectedfeatures', {'INPUT': self.effort_layer, 'OUTPUT': 'TEMPORARY_OUTPUT'})
            self.effort_LEG = algo_save['OUTPUT']

            # check if its glareSever or glareSeveri
            self.attr_effort = []
            for field in self.effort_LEG.fields():
                self.attr_effort.append(field.name())
            QgsMessageLog.logMessage('List of columns in effort file: {0}'.format(self.attr_effort))
            if 'glareSever' in self.attr_effort:
                self.name_glareS = 'glareSever'
            elif 'glareSeveri' in self.attr_effort:
                self.name_glareS = 'glareSeveri'
            QgsMessageLog.logMessage('Glare Severity: {0}'.format(self.name_glareS))


            self.totalSteps += 9

            self.check_effort()

        if self.obs_path != '':

            self.output_dir_obs = os.path.dirname(self.obs_path)
            QgsMessageLog.logMessage('OUTPUT DIRECTORY FOR OBSERVATION LOG: {0}'.format(self.output_dir_obs))

            # open files in qgis registry
            name_file = os.path.splitext(os.path.basename(self.obs_path))[0]
            self.obs_layer = self.open_shp(self.obs_path, 'check_'+name_file, False)

            self.totalSteps += 10

            self.check_obs()

        self.updateProgressBar()

    # -----------
    def check_effort(self):
        """
        Run checks for effort file
        :return:
        """

        # convert shp in csv to use pandas
        name_file = os.path.splitext(os.path.basename(self.effort_path))[0]
        path = os.path.join(self.output_dir_effort, name_file+'_df.csv')
        crs = QgsCoordinateReferenceSystem("EPSG:4326")

        save_options = QgsVectorFileWriter.SaveVectorOptions()
        save_options.driverName = "CSV"
        save_options.fileEncoding = "ascii"
        transform_context = QgsProject.instance().transformContext()
        writer, error_string = QgsVectorFileWriter.writeAsVectorFormatV2(
            self.effort_LEG, path, transform_context, save_options)  # ascii et non utf-8 pour encodage du °

        if writer != QgsVectorFileWriter.NoError:
            QgsMessageLog.logMessage('Writer error ({file}) : {details} '.format(
                file=path, details=error_string), level=Qgis.Warning)

        else:
            QgsMessageLog.logMessage('No writer error (%s).' % path, level=Qgis.Info)

        del writer

        self.effort_df = pd.read_csv(path)
        # enlever la colonne des index
        blankIndex = [''] * len(self.effort_df)
        self.effort_df.index = blankIndex

        df_unique = self.effort_df[['survey', 'computer', 'date', 'flight']].drop_duplicates()

        # création du fichier tx journal des logs
        self.path_effort = os.path.join(self.output_dir_effort, 'LOG_'+name_file+'.txt')
        if self.path_effort:
            f = open(self.path_effort, 'w', newline='')
            f.write('Log file of the automatic checks of the effort file ({0}). On {1}. \n\n {2}'.
                    format(os.path.basename(self.lbl_effort_path.text()), date.today().strftime("%d/%m/%Y"), df_unique))
            f.close()
        else:
            QMessageBox.critical(self, 'Error',
                                 'No txt file created.')

        self.check_envt_emty(self.effort_df)
        self.check_transect_glarechange(self.effort_df)
        self.check_1transect_glarechange(self.effort_df)
        self.check_glare_0(self.effort_df)
        self.check_observers(self.effort_df)
        self.check_seaState_subjective(self.effort_df)
        self.check_passage(self.effort_df)
        self.check_latlon(self.effort_df)
        self.check_comments(self.effort_df)

    # -----
    def check_envt_emty(self, layer):
        """
        Les champs seaState, swell, turbidity, skyglint, glareSever, cloudCover ne doivent pas être tous à 0 et
        les champs, glareFrom, glareTo, subjective ne doivent pas être tous vides.
        :return:
        """

        if self.name_glareS == 'glareSever':
            effort_col = layer.loc[:, ['survey', 'subRegion', 'transect', 'computer', 'routeType', 'effortGrp', 'effort',
                                       'status', 'date', 'hhmmss', 'seaState', 'swell', 'turbidity', 'skyGlint', 'glareFrom',
                                       'glareTo', 'glareSever', 'glareUnder', 'cloudCover', 'subjective', 'unexpLeft', 'unexpRight',
                                       'left', 'right', 'center', 'comment']]
        else:
            effort_col = layer.loc[:,
                         ['survey', 'subRegion', 'transect', 'computer', 'routeType', 'effortGrp', 'effort',
                          'status', 'date', 'hhmmss', 'seaState', 'swell', 'turbidity', 'skyGlint', 'glareFrom',
                          'glareTo', 'glareSeveri', 'glareUnder', 'cloudCover', 'subjective', 'unexpLeft', 'unexpRight',
                          'left', 'right', 'center', 'comment']]

        list_error_e = []
        for i in range(len(effort_col)):
            if effort_col["seaState"].iloc[i] == 0 and effort_col["swell"].iloc[i] == 0 and effort_col["turbidity"].iloc[i] == 0 \
                    and effort_col["skyGlint"].iloc[i] == 0 and effort_col[self.name_glareS].iloc[i] == 0 \
                    and effort_col["cloudCover"].iloc[i] == 0 \
                    and math.isnan(effort_col["glareFrom"].iloc[i]) and math.isnan(effort_col["glareTo"].iloc[i]) \
                    and math.isnan(effort_col["subjective"].iloc[i]):
                list_error_e.append((effort_col.iloc[[i]]).to_string(header=None))  # row, column

        nb_error_e = len(list_error_e)

        f = open(self.path_effort, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write('\n\n# ----- VERIFICATION OF ENVIRONMENTAL CONDITIONS \n'
                '[The fields seaState, swell, turbidity, skyglint, glareSever, cloudCover must not be '
                '0 and the fields glareFrom, glareTo, subjective must not be all empty.] \n'
                '> {0} errors \n'.format(nb_error_e))
        if nb_error_e > 0:
            f.write('{0} \n'.format(['survey', 'subRegion', 'transect', 'computer', 'routeType', 'effortGrp', 'effort',
                                   'status', 'date', 'hhmmss', 'seaState', 'swell', 'turbidity', 'skyGlint', 'glareFrom',
                                   'glareTo', 'glareSever', 'glareUnder', 'cloudCover', 'subjective', 'unexpLeft', 'unexpRight',
                                   'left', 'right', 'center', 'comment']))
            for i, elm in enumerate(list_error_e):
                f.write('{0} \n'.format(list_error_e[i]))
        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_e

    # -----
    def check_transect_glarechange(self, layer):
        """
        Lorsque l’on change de Transect, glareFrom et glareTo doivent changer de valeur (+/-30°).
        :return:
        """

        if self.name_glareS == 'glareSever':
            effort_col = layer.loc[:, ['survey', 'transect', 'flight', 'effortGrp', 'effort',
                                       'status', 'date', 'hhmmss', 'seaState', 'swell', 'turbidity', 'skyGlint',
                                       'glareFrom', 'glareTo', 'glareSever', 'glareUnder', 'cloudCover', 'subjective', 'comment']]
        else:
            effort_col = layer.loc[:, ['survey', 'transect', 'flight', 'effortGrp', 'effort',
                                       'status', 'date', 'hhmmss', 'seaState', 'swell', 'turbidity', 'skyGlint',
                                       'glareFrom', 'glareTo', 'glareSeveri', 'glareUnder', 'cloudCover', 'subjective',
                                       'comment']]

        effort_sort = effort_col.sort_values(by=['date', 'hhmmss'], ascending=True)

        list_error_gc = []
        list_rowerr_gc = []
        for i in range(len(effort_sort)):

            if effort_sort['survey'].iloc[i] == effort_sort['survey'].iloc[i-1] and \
                    effort_sort['date'].iloc[i] == effort_sort['date'].iloc[i - 1] and \
                    effort_sort['flight'].iloc[i] == effort_sort['flight'].iloc[i - 1] and \
                    effort_sort['transect'].iloc[i] != effort_sort['transect'].iloc[i-1] and \
                    effort_sort['glareFrom'].iloc[i - 1] + effort_sort['glareTo'].iloc[i - 1] != 0 and \
                    effort_sort['glareFrom'].iloc[i] + effort_sort['glareTo'].iloc[i] != 0:
                if (effort_sort['glareFrom'].iloc[i]
                    <= effort_sort['glareFrom'].iloc[i-1]-30
                    or effort_sort['glareFrom'].iloc[i]
                    >= effort_sort['glareFrom'].iloc[i - 1] + 30) and \
                        (effort_sort['glareTo'].iloc[i] <= effort_sort['glareTo'].iloc[i-1]-30
                         or effort_sort['glareTo'].iloc[i] >= effort_sort['glareTo'].iloc[i - 1] + 30):

                    continue

                else:
                    list_error_gc.append((effort_sort['transect'].iloc[i-1], effort_sort['transect'].iloc[i]))
                    list_rowerr_gc.append((effort_sort.iloc[[i-1, i]]).to_string(header=True))  # row, column

        # path_test = os.path.join(self.output_dir_effort, 'test_effort_df.txt')
        # self.effort_df.to_csv(path_test, header=None, index=None, sep='\t', mode='a')

        nb_error_gc = len(list_error_gc)

        f = open(self.path_effort, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF GLARE CHANGES BETWEEN TWO TRANSECTS \n"
                "[Between two different transects, glareFrom and glareTo must change their value (+/-30°).] \n"
                '> {0} errors \n'.format(nb_error_gc))

        if nb_error_gc > 0:
            f.write('Vérifier les changements entre les transects suivants: {0}\n'.format(list_error_gc))
            for i, elm in enumerate(list_rowerr_gc):
                f.write('{0} \n'.format(list_rowerr_gc[i]))
        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_gc

    # -----

    def check_1transect_glarechange(self, layer):
        """
        Sur un même transect, glareFrom et glareTo ne doivent pas avoir une valeur différente de
         +/-30° sauf si égal à 0.
        :return:
        """

        if self.name_glareS == 'glareSever':

            effort_col = layer.loc[:, ['survey', 'transect', 'flight', 'effortGrp', 'effort',
                                       'status', 'date', 'hhmmss', 'seaState', 'swell', 'turbidity', 'skyGlint',
                                       'glareFrom', 'glareTo', 'glareSever', 'glareUnder', 'cloudCover', 'subjective',
                                       'comment']]
        else:
            effort_col = layer.loc[:, ['survey', 'transect', 'flight', 'effortGrp', 'effort',
                                       'status', 'date', 'hhmmss', 'seaState', 'swell', 'turbidity', 'skyGlint',
                                       'glareFrom', 'glareTo', 'glareSeveri', 'glareUnder', 'cloudCover', 'subjective',
                                       'comment']]

        effort_sort = effort_col.sort_values(by=['date', 'hhmmss'], ascending=True)

        list_error_1gc = []
        list_rowerr_1gc = []
        for i in range(len(effort_sort)):

            if effort_sort['survey'].iloc[i] == effort_sort['survey'].iloc[i - 1] and \
                    effort_sort['date'].iloc[i] == effort_sort['date'].iloc[i - 1] and \
                    effort_sort['flight'].iloc[i] == effort_sort['flight'].iloc[i - 1] and \
                    effort_sort['transect'].iloc[i] == effort_sort['transect'].iloc[i - 1] and effort_sort['glareFrom'].iloc[i - 1] + effort_sort['glareTo'].iloc[i - 1] != 0 and effort_sort['glareFrom'].iloc[i] + effort_sort['glareTo'].iloc[i] != 0:

                if (effort_sort['glareFrom'].iloc[i] < effort_sort['glareFrom'].iloc[i - 1] - 30
                        or effort_sort['glareFrom'].iloc[i] > effort_sort['glareFrom'].iloc[i - 1] + 30) or \
                            (effort_sort['glareTo'].iloc[i] < effort_sort['glareTo'].iloc[i - 1] - 30
                             or effort_sort['glareTo'].iloc[i] > effort_sort['glareTo'].iloc[i - 1] + 30):

                    if (effort_sort['glareFrom'].iloc[i-1] > 330 or effort_sort['glareFrom'].iloc[i-1] <= 360) and effort_sort['glareFrom'].iloc[i] < 300:
                        if effort_sort['glareFrom'].iloc[i] + (360 - (effort_sort['glareFrom'].iloc[i-1])) > 30:
                            list_error_1gc.append(effort_sort['transect'].iloc[i])
                            list_rowerr_1gc.append(
                                    (effort_sort.iloc[[i - 1, i]]).to_string(header=True))  # row, column

                    elif (effort_sort['glareFrom'].iloc[i] > 330 or effort_sort['glareFrom'].iloc[i] <= 360) and effort_sort['glareFrom'].iloc[i-1] < 300:
                        if effort_sort['glareFrom'].iloc[i-1] + (360 - (effort_sort['glareFrom'].iloc[i])) > 30:
                            list_error_1gc.append(effort_sort['transect'].iloc[i])
                            list_rowerr_1gc.append((effort_sort.iloc[[i - 1, i]]).to_string(header=True))  # row, column

                    elif (effort_sort['glareTo'].iloc[i - 1] > 330 or effort_sort['glareTo'].iloc[i - 1] <= 360) and effort_sort['glareTo'].iloc[i] < 300:
                        if effort_sort['glareTo'].iloc[i] + (360 - (effort_sort['glareTo'].iloc[i - 1])) > 30:
                            list_error_1gc.append(effort_sort['transect'].iloc[i])
                            list_rowerr_1gc.append((effort_sort.iloc[[i - 1, i]]).to_string(header=True))  # row, column

                    elif (effort_sort['glareTo'].iloc[i] > 330 or effort_sort['glareTo'].iloc[i] <= 360) and \
                            effort_sort['glareTo'].iloc[i - 1] < 300:
                        if effort_sort['glareTo'].iloc[i - 1] + (360 - (effort_sort['glareTo'].iloc[i])) > 30:
                            list_error_1gc.append(effort_sort['transect'].iloc[i])
                            list_rowerr_1gc.append((effort_sort.iloc[[i - 1, i]]).to_string(header=True))  # row, column

                    else:
                        list_error_1gc.append(effort_sort['transect'].iloc[i])
                        list_rowerr_1gc.append((effort_sort.iloc[[i - 1, i]]).to_string(header=True))  # row, column

        # path_test = os.path.join(self.output_dir_effort, 'test_effort_df.txt')
        # self.effort_df.to_csv(path_test, header=None, index=None, sep='\t', mode='a')

        nb_error_1gc = len(list_error_1gc)

        f = open(self.path_effort, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF GLARE CHANGES IN A TRANSECT \n"
                "[Between two leg of the same transect, glareFrom and glareTo must not have a difference greater than "
                " +/-30° unless equal to 0.] \n"
                '> {0} errors \n'.format(nb_error_1gc))

        if nb_error_1gc > 0:
            f.write('Check for changes in the following transects: {0}\n'.format(list_error_1gc))
            for i, elm in enumerate(list_rowerr_1gc):
                f.write('{0} \n'.format(list_rowerr_1gc[i]))
        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_1gc

    # -----
    def check_glare_0(self, layer):
        """
        Si glareFrom et glareTo sont différents de 0 alors glareSever doit être différent de 0.
        ET Si glareSever est différent de 0, glareFrom et glareTo doivent être différents de 0.
        :return:
        """

        if self.name_glareS == 'glareSever':

            effort_col = layer.loc[:,
                         ['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp', 'effort',
                          'status', 'date', 'hhmmss', 'skyGlint',
                          'glareFrom', 'glareTo', 'glareSever', 'glareUnder', 'cloudCover', 'subjective',  'comment']]

        else:
            effort_col = layer.loc[:,
                         ['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp', 'effort',
                          'status', 'date', 'hhmmss', 'skyGlint',
                          'glareFrom', 'glareTo', 'glareSeveri', 'glareUnder', 'cloudCover', 'subjective', 'comment']]

        list_error_g0 = []
        for i in range(len(effort_col)):
            if (effort_col["glareFrom"].iloc[i] == 0 or math.isnan(effort_col["glareFrom"].iloc[i])) \
                    and (effort_col["glareTo"].iloc[i] == 0 or math.isnan(effort_col["glareTo"].iloc[i])) \
                    and (effort_col[self.name_glareS].iloc[i] != 0 or math.isnan(effort_col["glareTo"].iloc[i])):
                list_error_g0.append((effort_col.iloc[[i]]).to_string(header=None))  # row, column

            elif effort_col["glareFrom"].iloc[i] != 0 and effort_col["glareTo"].iloc[i] != 0 and effort_col[self.name_glareS].iloc[i] == 0:
                list_error_g0.append((effort_col.iloc[[i]]).to_string(header=None))  # row, column

        nb_error_g0 = len(list_error_g0)

        f = open(self.path_effort, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# -----  GLARE CHECK \n"
                '[If glareFrom and glareTo are different from 0 then glareSever must be different from 0. '
                'If glareSever is different from 0, glareFrom and glareTo must be different from 0.] \n'
                '> {0} errors \n'.format(nb_error_g0))
        if nb_error_g0 > 0:
            f.write('{0} \n'.format(['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp', 'effort',
                      'status', 'date', 'hhmmss', 'skyGlint',
                      'glareFrom', 'glareTo', 'glareSever', 'glareUnder', 'cloudCover', 'subjective', 'comment']))
            for i, elm in enumerate(list_error_g0):
                f.write('{0} \n'.format(list_error_g0[i]))
        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_g0

    # -----
    def check_observers(self, layer):
        """
        Sur un même transect et effortGrp les observateurs doivent rester à leur place :
        normalement la valeur de left et right ne doit pas changer.
        :return:
        """

        effort_col = layer.loc[:, ['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp', 'effort',
                                   'status', 'date', 'hhmmss', 'left', 'right', 'center', 'comment']]

        list_error_o = []
        for i in range(len(effort_col)):

            if effort_col['transect'].iloc[i] == effort_col['transect'].iloc[i-1] and \
                    effort_col['effortGrp'].iloc[i] == effort_col['effortGrp'].iloc[i-1]:
                if effort_col['left'].iloc[i] != effort_col['left'].iloc[i-1] \
                        or effort_col['right'].iloc[i] != effort_col['right'].iloc[i-1]:
                    list_error_o.append((effort_col.iloc[[i-1, i]]))  # row, column

        nb_error_o = len(list_error_o)

        f = open(self.path_effort, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF OBSERVER CHANGES \n"
                "[On the same transect and effortGrp the observers must stay in their place:"
                "the value of left and right should not change.] \n"
                '> {0} errors \n'.format(nb_error_o))
        if nb_error_o > 0:
            for i, elm in enumerate(list_error_o):
                f.write('{0} \n'.format(list_error_o[i]))
        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_o

    # -----
    def check_seaState_subjective(self, layer):
        """
        Pour un seaState de 3, subjective doit être différent de ’EE’, ‘GE’ ou ‘EG’.
        :return:
        """

        if self.name_glareS == 'glareSever':

            effort_col = layer.loc[:, ['survey', 'transect', 'computer', 'routeType', 'effortGrp', 'effort',
                                       'status', 'date', 'hhmmss',  'seaState', 'swell', 'turbidity', 'skyGlint',
                                       'glareFrom', 'glareTo', 'glareSever', 'glareUnder', 'cloudCover', 'subjective',
                                       'unexpLeft', 'unexpRight', 'left', 'right', 'center', 'comment']]
        else:
            effort_col = layer.loc[:, ['survey', 'transect', 'computer', 'routeType', 'effortGrp', 'effort',
                                       'status', 'date', 'hhmmss', 'seaState', 'swell', 'turbidity', 'skyGlint',
                                       'glareFrom', 'glareTo', 'glareSeveri', 'glareUnder', 'cloudCover', 'subjective',
                                       'unexpLeft', 'unexpRight', 'left', 'right', 'center', 'comment']]

        list_error_ss = []
        for i in range(len(effort_col)):

            if effort_col['seaState'].iloc[i] == 3 and \
                    (effort_col['subjective'].iloc[i] == 'EE' or
                     effort_col['subjective'].iloc[i] == 'EG' or
                     effort_col['subjective'].iloc[i] == 'GE'):
                list_error_ss.append((effort_col.iloc[[i]]).to_string(header=None))  # row, column

        nb_error_ss = len(list_error_ss)

        f = open(self.path_effort, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF THE STATE OF THE SEA \n"
                "[For a seaState of 3, subjective must be different from 'EE', 'GE' or 'EG'.] \n"
                '> {0} errors \n'.format(nb_error_ss))
        if nb_error_ss > 0:
            f.write('{0} \n'.format(['survey', 'transect', 'computer', 'routeType', 'effortGrp', 'effort',
                                   'status', 'date', 'hhmmss',  'seaState', 'swell', 'turbidity', 'skyGlint',
                                   'glareFrom', 'glareTo', 'glareSever', 'glareUnder', 'cloudCover', 'subjective',
                                   'unexpLeft', 'unexpRight', 'left', 'right', 'center', 'comment']))
            for i, elm in enumerate(list_error_ss):
                f.write('{0} \n'.format(list_error_ss[i]))
        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_ss

    # -----
    def check_passage(self, layer):
        """
        Vérifier les transects pour lesquels le passage est différent de 1.
        :return:
        """

        if self.name_glareS == 'glareSever':

            effort_col = layer.loc[:, ['survey', 'transect', 'passage', 'flight', 'computer', 'routeType', 'effortGrp',
                                       'effort', 'status', 'date', 'hhmmss', 'seaState', 'swell', 'turbidity', 'skyGlint',
                                       'glareFrom', 'glareTo', 'glareSever', 'glareUnder', 'cloudCover', 'subjective', 'comment']]
        else:
            effort_col = layer.loc[:, ['survey', 'transect', 'passage', 'flight', 'computer', 'routeType', 'effortGrp',
                                       'effort', 'status', 'date', 'hhmmss', 'seaState', 'swell', 'turbidity',
                                       'skyGlint', 'glareFrom', 'glareTo', 'glareSeveri', 'glareUnder', 'cloudCover', 'subjective',
                                       'comment']]

        list_passage_1 = []
        for i in range(len(effort_col)):

            if effort_col['passage'].iloc[i] != 1:
                list_passage_1.append((effort_col['flight'].iloc[i], effort_col['transect'].iloc[i]))

        nb_error_p = len(list_passage_1)
        set_passage_1_uo = set(list_passage_1)  # valeurs uniques
        set_passage_1 = sorted(set_passage_1_uo, key=lambda x: x[0])

        f = open(self.path_effort, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF THE NUMBER OF PASSAGES \n"
                "[Check the transects for which the number of passes is different from 1.] \n"
                '> {0} transects whose number of passages is different from 1. \n'.format(nb_error_p))

        if nb_error_p > 0:
            f.write('{0} \n'.format(set_passage_1))

        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_p

    # -----
    def check_latlon(self, layer):
        """
        Vérifier que la latitude et la longitude ne sont pas = 0 ou que lat =0
        :return:
        """

        effort_col = layer.loc[:, ['survey', 'transect', 'flight', 'date', 'hhmmss', 'lat', 'lon', 'comment']]

        list_latlon = []
        for i in range(len(effort_col)):

            if effort_col['lat'].iloc[i] == 0 or \
                    (effort_col['lat'].iloc[i] == 0 and effort_col['lon'].iloc[i] == 0):
                list_latlon.append((effort_col.iloc[[i]]).to_string(header=None))  # row, column

        nb_error_ll = len(list_latlon)

        f = open(self.path_effort, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF LATITUDES AND LONGITUDES \n"
                "[Check that the latitude and longitude are not equal to 0 or that the latitude only is not equal to 0.] \n"
                '> {0} errors. \n'.format(len(list_latlon)))

        if nb_error_ll > 0:
            f.write('{0} \n'.format(['survey', 'transect', 'flight', 'date', 'hhmmss', 'lat', 'lon', 'comment']))
            for i, elm in enumerate(list_latlon):
                f.write('{0} \n'.format(list_latlon[i]))

        f.close()

        self.step += 1
        self.updateProgressBar()

        return len(list_latlon)

    def check_comments(self, layer):
        """
        Vérifier que les commentaires corrigés ont bien été supprimés.
        :return:
        """

        effort_col = layer.loc[:, ['survey', 'transect', 'flight', 'computer', 'date', 'hhmmss', 'comment']]

        effort_comnona = effort_col.dropna(subset=['comment'])

        f = open(self.path_effort, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF COMMENTS \n"
                "[List lines including any comments.] \n"
                '> {0} commentaires. \n'.format(len(effort_comnona)))

        if len(effort_comnona) > 0:
            f.write('{0} \n'.format(effort_comnona))

        f.close()

        self.step += 1
        self.updateProgressBar()

        return len(effort_comnona)

# #####################################################################
# -----------
    def check_obs(self):
        """
        Run checks for observation file
        :return:
        """

        # convert shp in csv to use pandas
        name_file = os.path.splitext(os.path.basename(self.obs_path))[0]
        path = os.path.join(self.output_dir_obs, name_file+'_df.csv')
        crs = QgsCoordinateReferenceSystem("EPSG:4326")
        writer, error_string = QgsVectorFileWriter.writeAsVectorFormat(
            self.obs_layer, path, "ascii", crs, "CSV")  # ascii et non utf-8 pour encodage du °

        if writer != QgsVectorFileWriter.NoError:
            QgsMessageLog.logMessage('Writer error ({file}) : {details} '.format(
                file=path, details=error_string), level=Qgis.Warning)

        else:
            QgsMessageLog.logMessage('No writer error (%s).' % path, level=Qgis.Info)

        del writer

        self.obs_df = pd.read_csv(path)
        # enlever la colonne des index
        blankIndex = [''] * len(self.obs_df)
        self.obs_df.index = blankIndex

        df_unique = self.obs_df[['survey', 'computer', 'date', 'flight']].drop_duplicates()

        # création du fichier tx journal des logs
        self.path_obs = os.path.join(self.output_dir_obs, 'LOG_'+name_file+'.txt')
        if self.path_obs:
            f = open(self.path_obs, 'w', newline='')
            f.write("Log file of automatic checks of the observation file ({0}). On {1}. \n\n {2}".
                    format(os.path.basename(self.lbl_obs_path.text()), date.today().strftime("%d/%m/%Y"), df_unique))
            f.close()
        else:
            QMessageBox.critical(self, 'Error',
                                 'No txt file created.')

        self.check_age(self.obs_df)
        self.check_age_rempli(self.obs_df)
        self.check_decangle(self.obs_df)
        self.check_decangleoh(self.obs_df)
        self.check_cuebeha(self.obs_df)
        self.check_circleback(self.obs_df)
        self.check_decangle0(self.obs_df)
        self.check_trashbuoy(self.obs_df)
        self.check_latlon_obs(self.obs_df)
        self.check_cb(self.obs_df)

    # -----
    def check_age(self, layer):
        """
        Age uniquement rempli pour Species= SULBAS, LARGUL, BLAGUL, LARFUS, LARMAR, LARMIC, LARARG, LARCAN, LARMEL
         ou GREGUL sinon age == ‘NA’.
        :return:
        """

        obs_col = layer.loc[:, ['survey', 'transect', 'passage', 'flight', 'computer', 'routeType', 'effortGrp',
                                   'effort', 'status', 'date', 'hhmmss', 'species', 'age', 'observer', 'comment']]

        list_age = []
        for i in range(len(obs_col)):

            if obs_col['species'].iloc[i] != 'SULBAS' and \
                    obs_col['species'].iloc[i] != 'LARGUL' and \
                    obs_col['species'].iloc[i] != 'BLAGUL' and \
                    obs_col['species'].iloc[i] != 'LARFUS' and \
                    obs_col['species'].iloc[i] != 'LARMAR' and \
                    obs_col['species'].iloc[i] != 'LARMIC' and \
                    obs_col['species'].iloc[i] != 'LARARG' and \
                    obs_col['species'].iloc[i] != 'GREGUL' and \
                    obs_col['species'].iloc[i] != 'LARSPP' and \
                    obs_col['species'].iloc[i] != 'LARCAN' and \
                    obs_col['species'].iloc[i] != 'LARMEL' and \
                    not pd.isna(obs_col['age'].iloc[i]) and \
                    obs_col['age'].iloc[i] != '':
                list_age.append((obs_col.iloc[[i]]).to_string(header=None))  # row, column

        nb_error_a = len(list_age)

        f = open(self.path_obs, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF THE AGE \n"
                "[The age must be filled in only for the following species codes: "
                "SULBAS, LARGUL, BLAGUL, LARFUS, LARMAR, LARMIC, LARG, LARCAN, LARMEL or GREGUL.] \n"
                '> {0} errors. \n'.format(nb_error_a))

        if nb_error_a > 0:
            f.write('{0} \n'.format(['survey', 'transect', 'passage', 'flight', 'computer', 'routeType', 'effortGrp',
                                   'effort', 'status', 'date', 'hhmmss', 'species', 'age', 'observer', 'comment']))
            for i, elm in enumerate(list_age):
                f.write('{0} \n'.format(list_age[i]))

        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_a

    # -----
    def check_age_rempli(self, layer):
        """
        Age rempli pour groupe = Autre goeland, Goeland gris, Goeland noir, Moyen goeland gris, Fou. Ou age==NA ou
        'pas d'age' dans les commentaires.
        :return:
        """

        obs_col = layer.loc[:, ['survey', 'transect', 'passage', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'group', 'species', 'age', 'observer', 'comment']]

        list_age_r = []
        for i in range(len(obs_col)):

            if (obs_col['group'].iloc[i] == 'Autre goeland' or
                    obs_col['group'].iloc[i] == 'Goeland gris' or
                    obs_col['group'].iloc[i] == 'Goeland noir' or
                    obs_col['group'].iloc[i] == 'Moyen goeland gris' or
                    obs_col['group'].iloc[i] == 'Fou' or
                    obs_col['group'].iloc[i] == 'Other gull' or
                    obs_col['group'].iloc[i] == 'Grey gull' or
                    obs_col['group'].iloc[i] == 'Black gull' or
                    obs_col['group'].iloc[i] == 'Medium grey gull' or
                    obs_col['group'].iloc[i] == 'Booby') and \
                    (pd.isna(obs_col['age'].iloc[i]) or obs_col['age'].iloc[i] == ''):
                    # not pd.isna(obs_col["^PAS*AGE$"].iloc[i]) and
                    # obs_col['age'].iloc[i] == ''):
                list_age_r.append((obs_col.iloc[[i]]).to_string(header=None))  # row, column

        nb_error_ar = len(list_age_r)

        f = open(self.path_obs, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF THE AGE \n"
                "[The age must be filled in for the following groups: "
                "Other gull, Grey gull, Black gull, Medium grey gull, Booby] \n"
                '> {0} errors. \n'.format(nb_error_ar))

        if nb_error_ar > 0:
            f.write('{0} \n'.format(['survey', 'transect', 'passage', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'group', 'species', 'age', 'observer', 'comment']))
            for i, elm in enumerate(list_age_r):
                f.write('{0} \n'.format(list_age_r[i]))

        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_ar

    # -----

    def check_decangle(self, layer):
        """
        Angle > 10 pour Mammifères marins et Autre mégafaune 
        :return:
        """

        obs_col = layer.loc[:, ['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'taxon', 'species', 'decAngle', 'comment']]

        list_da = []
        for i in range(len(obs_col)):

            if ((obs_col['taxon'].iloc[i] == 'Mammifere marin'
                 or obs_col['taxon'].iloc[i] == 'Autre faune marine'
                 or obs_col['taxon'].iloc[i] == 'Marine mammal'
                 or obs_col['taxon'].iloc[i] == 'Other Marine Wildlife')
                and (obs_col['species'].iloc[i] != 'JELLY'
                and obs_col['species'].iloc[i] != 'SMAFIS'
                and obs_col['species'].iloc[i] != 'PLANCT')
                    and obs_col['decAngle'].iloc[i] <= 10):
                list_da.append((obs_col.iloc[[i]]).to_string(header=None))  # row, column

            elif obs_col['species'].iloc[i] == 'PLANCT' and obs_col['decAngle'].iloc[i] != 1:
                list_da.append((obs_col.iloc[[i]]).to_string(header=None))  # row, column

        nb_error_da = len(list_da)

        f = open(self.path_obs, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF THE DECANGLE FOR MARINE MAMMALS AND OTHER MARINE FAUNA \n"
                "[The decAngle must be greater than 10 for marine mammals and other marine fauna (excluding JELLY, PLANCT and SMAFIS).] \n"
                '> {0} errors. \n'.format(nb_error_da))

        if nb_error_da > 0:
            f.write('{0} \n'.format(['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'taxon', 'species', 'decAngle', 'comment']))
            for i, elm in enumerate(list_da):
                f.write('{0} \n'.format(list_da[i]))

        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_da

    # ----
    def check_decangleoh(self, layer):
        """
        Pour les oiseaux  ou activités humaines, angle = 1 / 3 sauf pour les Océanites et Passer angle 1 seulement
        :return:
        """

        obs_col = layer.loc[:, ['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'taxon', 'group', 'family', 'species', 'decAngle', 'comment']]

        list_daoh = []
        for i in range(len(obs_col)):

            if ((obs_col['taxon'].iloc[i] == 'Oiseau marin' or
                obs_col['taxon'].iloc[i] == 'Seabird' or
                obs_col['taxon'].iloc[i] == 'Oiseau cotier' or
                obs_col['taxon'].iloc[i] == 'Coastal Bird' or
                obs_col['taxon'].iloc[i] == 'Oiseau terrestre' or
                obs_col['taxon'].iloc[i] == 'Land Bird') and
                (obs_col['decAngle'].iloc[i] != 1 and
                 obs_col['decAngle'].iloc[i] != 3)) or ((obs_col['group'].iloc[i] == 'Oceanite' or
                  obs_col['species'].iloc[i] == 'PASSER') and
                 obs_col['decAngle'].iloc[i] != 1):
                list_daoh.append((obs_col.iloc[[i]]).to_string(header=None))  # row, column


        nb_error_daoh = len(list_daoh)

        f = open(self.path_obs, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF THE DECANGLE FOR BIRDS AND HUMAN ACTIVITIES\n"
                "[The decAngle should be equal to 1 for Oceanites and passerines and 1 or 3 for other birds and human activities. ] \n"
                '> {0} errors. \n'.format(nb_error_daoh))

        if nb_error_daoh > 0:
            f.write('{0} \n'.format(['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'taxon', 'group', 'family', 'species', 'decAngle', 'comment']))
            for i, elm in enumerate(list_daoh):
                f.write('{0} \n'.format(list_daoh[i]))

        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_daoh

    # ----
    def check_cuebeha(self, layer):
        """
        Pour les Mammifères marins vérifier Cue != ‘ ‘   +  Behaviour != ‘ ‘ + Calves != ''
        :return:
        """

        obs_col = layer.loc[:, ['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp', 'effort',
                                'status', 'date', 'hhmmss', 'taxon', 'species', 'cue', 'behaviour', 'swimDir', 'calves', 'comment']]

        list_cb = []
        for i in range(len(obs_col)):

            if (obs_col['taxon'].iloc[i] == 'Mammifere marin' or obs_col['taxon'].iloc[i] == 'Marine mammal') and \
                    (pd.isna(obs_col['cue'].iloc[i]) or pd.isna(obs_col['behaviour'].iloc[i]) or pd.isna(obs_col['calves'].iloc[i])):
                list_cb.append((obs_col.iloc[[i]]).to_string(header=None))  # row, column

        nb_error_cb = len(list_cb)

        f = open(self.path_obs, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF MARINE MAMMALS \n"
                "[For Marine Mammals, check that the columns 'cue', 'calves' and 'behaviour' are filled in.] \n"
                '> {0} errors. \n'.format(nb_error_cb))

        if nb_error_cb > 0:
            f.write('{0} \n'.format(['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp', 'effort',
                                'status', 'date', 'hhmmss', 'taxon', 'species', 'cue', 'behaviour', 'swimDir', 'calves', 'comment']))
            for i, elm in enumerate(list_cb):
                f.write('{0} \n'.format(list_cb[i]))

        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_cb

    # ----
    def check_circleback(self, layer):
        """
        Vérifier le Status de l’observation pour les circle-back et s'il y a des observations hors effort pendant le CB.
        :return:
        """

        obs_col = layer.loc[:, ['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'taxon', 'species', 'status',
                                'comment']]

        list_c = []
        for i in range(len(obs_col)):

            if obs_col['routeType'].iloc[i] == 'CIRCLE BACK':
                list_c.append((obs_col.iloc[[i]]).to_string(header=None))  # row, column

        nb_error_c = len(list_c)

        f = open(self.path_obs, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF CIRCLE BACKS \n"
                "[Check for off-effort observations during a circle back. "
                "If so, check the status of the observation.] \n"
                '> {0} errors. \n'.format(nb_error_c))

        if nb_error_c > 0:
            f.write('{0} \n'.format(['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'taxon', 'species', 'status',
                                'comment']))
            for i, elm in enumerate(list_c):
                f.write('{0} \n'.format(list_c[i]))

        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_c

    # ----
    def check_decangle0(self, layer):
        """
        Vérifier que pour les decAngle ==0, comments contient ‘no angle’. Uniquement sur les routeType LEG.
        :return:
        """

        obs_col = layer.loc[:, ['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'taxon', 'species', 'decAngle',
                                'comment']]

        list_da0 = []
        for i in range(len(obs_col)):

            if obs_col['routeType'].iloc[i] == 'LEG' \
                    and obs_col['decAngle'].iloc[i] == 0 \
                    and 'no angle' not in str(obs_col['comment'].iloc[i]).lower():
                list_da0.append((obs_col.iloc[[i]]).to_string(header=None))  # row, column

        nb_error_da0 = len(list_da0)

        f = open(self.path_obs, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF THE DECANGLE 0 \n"
                "[Check that the comment contains 'no angle' for decAngle of 0."
                "Only on LEG routeType.] \n"
                '> {0} errors. \n'.format(nb_error_da0))

        if nb_error_da0 > 0:
            f.write('{0} \n'.format(['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'taxon', 'species', 'decAngle',
                                'comment']))
            for i, elm in enumerate(list_da0):
                f.write('{0} \n'.format(list_da0[i]))

        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_da0

    # ----
    def check_trashbuoy(self, layer):
        """
        Si Family == ‘Trash’ or ‘Buoy' or Ship alors decAngle == 1
        :return: nb_error_tb
        """

        obs_col = layer.loc[:, ['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'taxon',  'family', 'species',
                                'decAngle', 'comment']]

        list_tb = []
        for i in range(len(obs_col)):

            if obs_col['routeType'].iloc[i] == 'LEG' \
                    and obs_col['decAngle'].iloc[i] != 1 \
                    and (obs_col['family'].iloc[i] == 'Trash' or
                         obs_col['family'].iloc[i] == 'Buoy' or
                         obs_col['family'].iloc[i] == 'Dechet' or
                         obs_col['family'].iloc[i] == 'Bouee' or
                         obs_col['family'].iloc[i] == 'Bateau' or
                         obs_col['family'].iloc[i] == 'Ship'):
                list_tb.append((obs_col.iloc[[i]]).to_string(header=None))  # row, column

        nb_error_tb = len(list_tb)

        f = open(self.path_obs, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF WASTE, BOATS AND BUOY \n"
                "[For waste, boats and buoys, the decAngle must be 1.] \n"
                '> {0} errors. \n'.format(nb_error_tb))

        if nb_error_tb > 0:
            f.write('{0} \n'.format(['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'taxon',  'family', 'species',
                                'decAngle', 'comment']))
            for i, elm in enumerate(list_tb):
                f.write('{0} \n'.format(list_tb[i]))

        f.close()

        self.step += 1
        self.updateProgressBar()

        return nb_error_tb

    # -----
    def check_latlon_obs(self, layer):
        """
        Vérifier que la latitude et la longitude ne sont pas = 0 ou que lat =0
        :return:
        """

        obs_col = layer.loc[:, ['survey', 'transect', 'flight', 'date', 'hhmmss', 'lat', 'lon', 'comment']]

        list_latlon = []
        for i in range(len(obs_col)):

            if obs_col['lat'].iloc[i] == 0 or \
                    (obs_col['lat'].iloc[i] == 0 and obs_col['lon'].iloc[i] == 0):
                list_latlon.append((obs_col.iloc[[i]]).to_string(header=None))  # row, column

        nb_error_ll = len(list_latlon)

        f = open(self.path_obs, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- VERIFICATION OF LATITUDES AND LONGITUDES \n"
                "[Check that the latitude and longitude are not equal to 0 or that the latitude only is not equal to 0.] \n"
                '> {0} errors. \n'.format(len(list_latlon)))

        if nb_error_ll > 0:
            f.write('{0} \n'.format(['survey', 'transect', 'flight', 'date', 'hhmmss', 'lat', 'lon', 'comment']))
            for i, elm in enumerate(list_latlon):
                f.write('{0} \n'.format(list_latlon[i]))

        f.close()

        self.step += 1
        self.updateProgressBar()

        return len(list_latlon)

    # -----
    def check_cb(self, layer):
        """
        Lister les observations liées à un CB
        :return:
        """

        obs_col = layer.loc[:, ['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'taxon',  'family', 'species',
                                'comment']]

        list_cb = []
        for i in range(len(obs_col)):

            if obs_col['status'].iloc[i] == 'CB':
                list_cb.append((obs_col.iloc[[i]]).to_string(header=None))  # row, column

        nb_error_cb = len(list_cb)

        f = open(self.path_obs, 'a')  # a pour écrire à la suite et pas tout écraser
        f.write("\n\n# ----- CIRCLE BACK CHECK \n"
                "[Check for observations with a CircleBack status.] \n"
                '> {0} errors. \n'.format(len(list_cb)))

        if nb_error_cb > 0:
            f.write('{0} \n'.format(['survey', 'transect', 'flight', 'computer', 'routeType', 'effortGrp',
                                'effort', 'status', 'date', 'hhmmss', 'taxon',  'family', 'species',
                                'comment']))
            for i, elm in enumerate(list_cb):
                f.write('{0} \n'.format(list_cb[i]))

        f.close()

        self.step += 1
        self.updateProgressBar()

        return len(list_cb)